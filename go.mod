module gitlab.com/slagit/fakedo

go 1.14

require (
	github.com/google/uuid v1.2.0
	github.com/julienschmidt/httprouter v1.3.0
	golang.org/x/crypto v0.0.0-20210220033148-5ea612d1eb83
	gopkg.in/yaml.v2 v2.4.0
)
