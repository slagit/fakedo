# API Simulator for Digital Ocean

This project contains a simulator that simulates the Digitial Ocean
API. It is intended for NON PRODUCTION use in test environments to help
test/debug software without hitting the real Digital Ocean API.

This project aims to similate the API in a "good enough" manner, and
does not claim to completely accurately represent the API. Additionally,
large portions of the API are not implemented.

This project is not assoicated with, or sponsered by, Digital Ocean in any way.

## Installation

```sh
go build -o fakedo cmd/fakedo/main.go
sudo install -o 0 -g 0 fakedo /usr/local/bin/
```

## Running

See `test/config/fakedo-config.json` for a sample configuration file.

```sh
fakedo
```
