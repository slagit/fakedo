package api

import (
	"context"
	"errors"
	"net/http"
	"strings"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/slagit/fakedo/core"
	"golang.org/x/crypto/ssh"
)

func parseKey(key string) (ssh.PublicKey, error) {
	parts := strings.SplitN(key, " ", 2)
	if len(parts) == 1 {
		return nil, errors.New("Key invalid, key should be of the format `type key [comment]`")
	}
	switch parts[0] {
	case "ssh-rsa", "ssh-dss", "ecdsa-sha2-nistp256", "ecdsa-sha2-nistp384", "ecdsa-sha2-nistp521", "ssh-ed25519":
		// Normal, supported format
	default:
		return nil, errors.New("Key invalid type, we support 'ssh-rsa', 'ssh-dss', 'ecdsa-sha2-nistp256', 'ecdsa-sha2-nistp384', 'ecdsa-sha2-nistp521', or 'ssh-ed25519'")
	}

	pk, _, _, _, err := ssh.ParseAuthorizedKey([]byte(key))
	if err != nil {
		return nil, errors.New("Fingerprint could not be generated, please make sure your key is valid")
	}

	return pk, nil
}

func (api *APIImpl) CreateSSHKey(ctx context.Context, response http.ResponseWriter, request *http.Request, ps httprouter.Params) {
	var key core.SSHKey
	err := api.decodeBody(response, request, &key)
	if err != nil {
		return
	}

	pk, err := parseKey(key.PublicKey)
	if err != nil {
		api.unprocessableEntity(response, err.Error())
		return
	}

	key.Fingerprint = ssh.FingerprintLegacyMD5(pk)

	key = api.db.AddSSHKey(&key)

	api.log.Printf("There are now %d ssh keys.", api.db.CountSSHKeys())

	result := struct {
		SSHKey core.SSHKey `json:"ssh_key"`
	}{
		SSHKey: key,
	}
	err = api.writeJSONResponse(response, http.StatusCreated, result)
	if err != nil {
		api.internalError(response)
		return
	}
}

func (api *APIImpl) ListSSHKeys(ctx context.Context, response http.ResponseWriter, request *http.Request, _ httprouter.Params) {
	keys := api.db.GetSSHKeys()
	result, err := paginate(SSHKeyList(keys), "ssh_keys", request)
	if err != nil {
		api.internalError(response)
		return
	}
	err = api.writeJSONResponse(response, http.StatusOK, result)
	if err != nil {
		api.internalError(response)
		return
	}
}
