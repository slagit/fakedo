package api

import (
	"context"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

func (api *APIImpl) ListVPCs(ctx context.Context, response http.ResponseWriter, request *http.Request, _ httprouter.Params) {
	vpcs := api.db.GetVPCs()
	result, err := paginate(VPCList(vpcs), "vpcs", request)
	if err != nil {
		api.internalError(response)
		return
	}
	err = api.writeJSONResponse(response, http.StatusOK, result)
	if err != nil {
		api.internalError(response)
		return
	}
}
