package api

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"net/url"
	"strconv"

	"github.com/julienschmidt/httprouter"

	"gitlab.com/slagit/fakedo/core"
	"gitlab.com/slagit/fakedo/dns"
)

func (api *APIImpl) CreateDomainRecord(ctx context.Context, response http.ResponseWriter, request *http.Request, ps httprouter.Params) {
	domainName := ps.ByName("domain_name")
	domain := api.db.GetDomain(domainName)
	if domain == nil {
		api.log.Printf("Unknown domain: %s\n", domainName)
		response.WriteHeader(http.StatusNotFound)
		return
	}

	var record core.DomainRecord
	err := api.decodeBody(response, request, &record)
	if err != nil {
		return
	}

	if record.Name != "@" {
		err = dns.Validate(record.Name)
		if err != nil {
			api.unprocessableEntity(response, fmt.Sprintf("Name %s", err))
			return
		}
	}

	if record.Type == "TXT" {
		record.Priority = nil
		record.Port = nil
		record.Weight = nil
		record.Flags = nil
		record.Tag = nil
	} else if record.Type == "A" {
		ip := net.ParseIP(record.Data)
		if ip == nil || ip.To4() == nil {
			api.unprocessableEntity(response, "IP address did not match IPv4 format (e.g. 127.0.0.1).")
			return
		}
		record.Priority = nil
		record.Port = nil
		record.Weight = nil
		record.Flags = nil
		record.Tag = nil
	} else {
		api.unprocessableEntity(response, fmt.Sprintf("Type unsupported: %s", record.Type))
		return
	}
	if record.TTL == nil {
		record.TTL = new(uint32)
		*record.TTL = domain.TTL
	} else if *record.TTL < 30 {
		api.unprocessableEntity(response, "TTL to short")
		return
	}

	record = api.db.AddDomainRecord(domainName, &record)

	api.log.Printf("There are now %d domains with %d records.", api.db.CountDomains(), api.db.CountDomainRecords())

	result := struct {
		DomainRecord core.DomainRecord `json:"domain_record"`
	}{
		DomainRecord: record,
	}
	err = api.writeJSONResponse(response, http.StatusCreated, result)
	if err != nil {
		api.internalError(response)
		return
	}
}

func (api *APIImpl) DeleteDomainRecord(ctx context.Context, response http.ResponseWriter, request *http.Request, ps httprouter.Params) {
	domainName := ps.ByName("domain_name")
	domain := api.db.GetDomain(domainName)
	if domain == nil {
		api.log.Printf("Unknown domain: %s\n", domainName)
		response.WriteHeader(http.StatusNotFound)
		return
	}

	recordIdStr := ps.ByName("record_id")
	recordId, err := strconv.ParseInt(recordIdStr, 10, 64)
	if err != nil {
		api.log.Printf("Invalid record id: %s\n", recordIdStr)
		response.WriteHeader(http.StatusNotFound)
		return
	}
	record := api.db.DeleteDomainRecord(domainName, recordId)
	if record == nil {
		api.log.Printf("Unknown domain record: %d\n", recordId)
		response.WriteHeader(http.StatusNotFound)
		return
	}

	api.log.Printf("There are now %d domains with %d records.", api.db.CountDomains(), api.db.CountDomainRecords())

	response.WriteHeader(http.StatusNoContent)
}

func (api *APIImpl) ListDomainRecords(ctx context.Context, response http.ResponseWriter, request *http.Request, ps httprouter.Params) {
	domainName := ps.ByName("domain_name")
	domain := api.db.GetDomain(domainName)
	if domain == nil {
		api.log.Printf("Unknown domain: %s\n", domainName)
		response.WriteHeader(http.StatusNotFound)
		return
	}

	query, err := url.ParseQuery(request.URL.RawQuery)
	if err != nil {
		api.log.Printf("Invalid query: %s\n", request.URL.RawQuery)
		response.WriteHeader(http.StatusBadRequest)
		return
	}

	var matchName *string
	if val, ok := query["name"]; ok {
		matchName = &val[0]
	}
	var matchType *string
	if val, ok := query["type"]; ok {
		matchType = &val[0]
	}

	records := api.db.GetDomainRecords(domainName)

	filteredRecords := records
	if matchName != nil || matchType != nil {
		filteredRecords = []core.DomainRecord{}
		for _, record := range records {
			if matchName != nil {
				realName := domainName
				if record.Name != "@" {
					realName = fmt.Sprintf("%s.%s", record.Name, domainName)
				}
				if *matchName != realName {
					continue
				}
			}
			if matchType != nil && *matchType != record.Type {
				continue
			}
			filteredRecords = append(filteredRecords, record)
		}
	}

	result, err := paginate(DomainRecordList(filteredRecords), "domain_records", request)
	if err != nil {
		api.internalError(response)
		return
	}
	err = api.writeJSONResponse(response, http.StatusOK, result)
	if err != nil {
		api.internalError(response)
		return
	}
}
