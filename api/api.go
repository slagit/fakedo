package api

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/url"

	"github.com/julienschmidt/httprouter"

	"gitlab.com/slagit/fakedo/db"
	"gopkg.in/yaml.v2"
)

type apiHandlerFunc func(context.Context, http.ResponseWriter, *http.Request, httprouter.Params)

func (f apiHandlerFunc) ServeHTTP(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	f(r.Context(), w, r, ps)
}

type apiHandler interface {
	ServeHTTP(w http.ResponseWriter, r *http.Request)
}

type APIImpl struct {
	log *log.Logger
	db  *db.Database
}

func New(log *log.Logger, db *db.Database) *APIImpl {
	return &APIImpl{
		log: log,
		db:  db,
	}
}

func (api *APIImpl) HandleFunc(handler apiHandlerFunc) httprouter.Handle {
	return func(response http.ResponseWriter, request *http.Request, ps httprouter.Params) {
		az, err := api.checkAuthorization(response, request)
		if err != nil {
			api.internalError(response)
			return
		}
		if !az {
			return
		}
		api.log.Printf("%s %s -> calling handler()\n", request.Method, request.URL.Path)
		handler.ServeHTTP(response, request, ps)
	}
}

func (api *APIImpl) DoctlConfig(response http.ResponseWriter, request *http.Request, ps httprouter.Params) {
	b, err := yaml.Marshal(struct {
		APIURL       string            `yaml:"api-url"`
		AuthContexts map[string]string `yaml:"auth-contexts"`
		Context      string
	}{
		APIURL: (&url.URL{
			Scheme: "https",
			Host:   request.Host,
		}).String(),
		AuthContexts: map[string]string{"fakedo": "blah"},
		Context:      "fakedo",
	})
	if err != nil {
		api.internalError(response)
		return
	}
	response.Write(b)
}

func (api *APIImpl) Handler() http.Handler {
	m := httprouter.New()
	m.GET("/oob/doctl/config.yaml", api.DoctlConfig)
	m.GET("/v2/domains/", api.HandleFunc(api.ListDomains))
	m.POST("/v2/domains/", api.HandleFunc(api.CreateDomain))
	m.GET("/v2/domains/:domain_name/records/", api.HandleFunc(api.ListDomainRecords))
	m.POST("/v2/domains/:domain_name/records/", api.HandleFunc(api.CreateDomainRecord))
	m.DELETE("/v2/domains/:domain_name/records/:record_id", api.HandleFunc(api.DeleteDomainRecord))
	m.GET("/v2/droplets/", api.HandleFunc(api.ListDroplets))
	m.POST("/v2/droplets/", api.HandleFunc(api.CreateDroplet))
	m.GET("/v2/droplets/:droplet_id", api.HandleFunc(api.GetDroplet))
	m.DELETE("/v2/droplets/:droplet_id", api.HandleFunc(api.DeleteDroplet))
	m.GET("/v2/images/", api.HandleFunc(api.ListImages))
	m.POST("/v2/images/", api.HandleFunc(api.CreateImage))
	m.GET("/v2/account/keys/", api.HandleFunc(api.ListSSHKeys))
	m.POST("/v2/account/keys/", api.HandleFunc(api.CreateSSHKey))
	m.GET("/v2/vpcs/", api.HandleFunc(api.ListVPCs))
	return m
}

func (api *APIImpl) checkAuthorization(response http.ResponseWriter, request *http.Request) (bool, error) {
	if request.Header.Get("Authorization") != "Bearer blah" {
		err := api.writeJSONResponse(response, http.StatusUnauthorized, map[string]string{
			"id":      "unauthorized",
			"message": "unable to authenticate you.",
		})
		return false, err
	}
	return true, nil
}

func (api *APIImpl) decodeBody(response http.ResponseWriter, request *http.Request, obj interface{}) error {
	dec := json.NewDecoder(request.Body)
	err := dec.Decode(obj)
	if err != nil {
		api.unprocessableEntity(response, fmt.Sprintf("Error decoding request body: %s", err))
		return err
	}
	return nil
}

func (api *APIImpl) badRequest(response http.ResponseWriter, msg string) {
	api.log.Printf(msg)
	err := api.writeJSONResponse(response, http.StatusBadRequest, map[string]string{
		"id":      "bad_request",
		"message": msg,
	})
	if err != nil {
		api.internalError(response)
	}
}

func (api *APIImpl) internalError(response http.ResponseWriter) {
	err := api.writeJSONResponse(response, http.StatusUnauthorized, map[string]string{
		"id":      "internal_server_error",
		"message": "internal server error.",
	})
	if err != nil {
		api.log.Printf("Error writing internal error: %s\n", err)
	}
}

func (api *APIImpl) notFound(response http.ResponseWriter, msg string) {
	err := api.writeJSONResponse(response, http.StatusNotFound, map[string]string{
		"id":      "not_found",
		"message": msg,
	})
	if err != nil {
		api.log.Printf("Error writing internal error: %s\n", err)
	}
}

func (api *APIImpl) unprocessableEntity(response http.ResponseWriter, msg string) {
	api.log.Printf(msg)
	err := api.writeJSONResponse(response, http.StatusUnprocessableEntity, map[string]string{
		"id":      "unprocessable_entity",
		"message": msg,
	})
	if err != nil {
		api.internalError(response)
	}
}

func (api *APIImpl) writeJSONResponse(response http.ResponseWriter, status int, v interface{}) error {
	jsonReply, err := marshalIndent(v)
	if err != nil {
		return err
	}

	response.Header().Set("Content-Type", "application/json; charset=utf-8")
	response.WriteHeader(status)

	_, _ = response.Write(jsonReply)
	return nil
}

func marshalIndent(v interface{}) ([]byte, error) {
	return json.MarshalIndent(v, "", "  ")
}
