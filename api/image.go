package api

import (
	"context"
	"net/http"
	"time"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/slagit/fakedo/core"
)

func (api *APIImpl) CreateImage(ctx context.Context, response http.ResponseWriter, request *http.Request, ps httprouter.Params) {
	var image core.Image
	err := api.decodeBody(response, request, &image)
	if err != nil {
		return
	}

	if image.URL == "" {
		api.badRequest(response, "Your source URL is unreachable")
		return
	}
	image.URL = ""

	switch image.Distribution {
	case "Arch Linux", "CentOS", "CoreOS", "Debian", "Fedora", "Fedora Atomic", "FreeBSD", "Gentoo", "openSUSE", "RancherOS", "Ubuntu":
		// Distribution is valid
	case "":
		api.badRequest(response, "Your request is missing an image distribution")
		return
	default:
		image.Distribution = "Unknown OS"
	}

	region := image.Region
	image.Region = ""

	round, err := time.ParseDuration("1s")
	if err != nil {
		panic(err)
	}
	image.CreatedAt = time.Now().Round(round)
	image.UpdatedAt = image.CreatedAt
	image.ErrorMessage = ""
	image.Status = core.ImageStatusNew
	image.Type = core.ImageTypeCustom
	image.Regions = []string{}
	if image.Tags == nil {
		image.Tags = []string{}
	}

	image = api.db.AddImage(&image)
	if region != "fake1" {
		api.db.DeleteImage(image.ID)
	}

	go func(id int64, region string) {
		time.Sleep(5 * time.Second)
		image := api.db.GetImage(id)
		if image != nil {
			if image.Status == core.ImageStatusNew {
				image.Status = core.ImageStatusPending
				image.UpdatedAt = time.Now().Round(round)
				api.db.UpdateImage(image)
			}
			time.Sleep(5 * time.Second)
			image = api.db.GetImage(id)
			if image != nil {
				image.Regions = []string{region}
				image.Status = core.ImageStatusAvailable
				image.UpdatedAt = time.Now().Round(round)
				api.db.UpdateImage(image)
			}
		}
	}(image.ID, region)

	api.log.Printf("There are now %d images.", api.db.CountImages())

	result := struct {
		Image core.Image `json:"image"`
	}{
		Image: image,
	}
	err = api.writeJSONResponse(response, http.StatusAccepted, result)
	if err != nil {
		api.internalError(response)
		return
	}
}

func (api *APIImpl) ListImages(ctx context.Context, response http.ResponseWriter, request *http.Request, _ httprouter.Params) {
	images := api.db.GetImages()
	result, err := paginate(ImageList(images), "images", request)
	if err != nil {
		api.internalError(response)
		return
	}
	err = api.writeJSONResponse(response, http.StatusOK, result)
	if err != nil {
		api.internalError(response)
		return
	}
}
