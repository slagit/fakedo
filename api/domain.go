package api

import (
	"context"
	"fmt"
	"net/http"

	"github.com/julienschmidt/httprouter"

	"gitlab.com/slagit/fakedo/core"
	"gitlab.com/slagit/fakedo/dns"
)

func (api *APIImpl) CreateDomain(ctx context.Context, response http.ResponseWriter, request *http.Request, ps httprouter.Params) {
	var domain core.Domain
	err := api.decodeBody(response, request, &domain)
	if err != nil {
		return
	}

	err = dns.Validate(domain.Name)
	if err != nil {
		api.unprocessableEntity(response, fmt.Sprintf("Name %s", err))
		return
	}
	domain.TTL = 3600

	if api.db.GetDomain(domain.Name) != nil {
		api.unprocessableEntity(response, "Name already exists")
		return
	}

	domain = api.db.AddDomain(&domain)

	api.log.Printf("There are now %d domains with %d records.", api.db.CountDomains(), api.db.CountDomainRecords())

	result := struct {
		Domain core.Domain `json:"domain"`
	}{
		Domain: domain,
	}
	err = api.writeJSONResponse(response, http.StatusCreated, result)
	if err != nil {
		api.internalError(response)
		return
	}
}

func (api *APIImpl) ListDomains(ctx context.Context, response http.ResponseWriter, request *http.Request, _ httprouter.Params) {
	domains := api.db.GetDomains()
	result, err := paginate(DomainList(domains), "domains", request)
	if err != nil {
		api.internalError(response)
		return
	}
	err = api.writeJSONResponse(response, http.StatusOK, result)
	if err != nil {
		api.internalError(response)
		return
	}
}
