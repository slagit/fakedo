package api

import (
	"context"
	"encoding/binary"
	"math/rand"
	"net"
	"net/http"
	"strconv"
	"time"

	"github.com/julienschmidt/httprouter"
	"gitlab.com/slagit/fakedo/core"
)

type createDroplet struct {
	Name              string        `json:"name"`
	Region            string        `json:"region"`
	Size              string        `json:"size"`
	Image             interface{}   `json:"image"`
	SSHKeys           []interface{} `json:"ssh_keys"`
	Backups           bool          `json:"backups"`
	IPv6              bool          `json:"ipv6"`
	PrivateNetworking bool          `json:"private_networking"`
	VPCUUID           string        `json:"vpc_uuid"`
	UserData          string        `json:"user_data"`
	Monitoring        bool          `json:"monitoring"`
	Volumes           []interface{} `json:"volumes,omitempty"`
	Tags              []string      `json:"tags"`
}

func randomNetwork(cidr, t string) (*core.DropletNetwork, error) {
	gateway, ipn, err := net.ParseCIDR(cidr)
	if err != nil {
		return nil, err
	}

	ip := make([]byte, 4)
	binary.LittleEndian.PutUint32(ip, rand.Uint32())
	for i := range ip {
		ip[i] = (ipn.IP[i] & ipn.Mask[i]) | (ip[i] &^ ipn.Mask[i])
	}

	if t != core.DropletNetworkTypePublic {
		gateway = []byte{}
	}

	return &core.DropletNetwork{
		IPAddress: net.IP(ip).String(),
		Netmask:   net.IP(ipn.Mask).String(),
		Gateway:   gateway.String(),
		Type:      t,
	}, nil
}

func (api *APIImpl) CreateDroplet(ctx context.Context, response http.ResponseWriter, request *http.Request, ps httprouter.Params) {
	var createDroplet createDroplet
	err := api.decodeBody(response, request, &createDroplet)
	if err != nil {
		return
	}

	var droplet core.Droplet

	if createDroplet.Name == "" {
		api.unprocessableEntity(response, "Droplet must have a name")
		return
	}
	droplet.Name = createDroplet.Name

	if createDroplet.Region != "fake1" {
		api.unprocessableEntity(response, "You specified an invalid region for Droplet creation.")
		return
	}
	droplet.Region = core.Region{
		Name:      "Fake 1",
		Slug:      "fake1",
		Features:  []string{},
		Available: true,
		Sizes: []string{
			"s-1vcpu-1gb",
		},
	}

	if createDroplet.Size != "s-1vcpu-1gb" {
		api.unprocessableEntity(response, "You specified an invalid size for Droplet creation.")
		return
	}
	droplet.Size = core.Size{
		Slug:         "s-1vcpu-1gb",
		Memory:       1024,
		VCPUs:        1,
		Disk:         25,
		Transfer:     1,
		PriceMonthly: 5,
		PriceHourly:  0.00744,
		Regions: []string{
			"fake1",
		},
		Available: true,
	}
	droplet.SizeSlug = "s-1vcpu-1gb"
	droplet.Memory = 1024
	droplet.VCPUs = 1
	droplet.Disk = 25

	switch image_key := createDroplet.Image.(type) {
	case string:
		api.unprocessableEntity(response, "You specified an invalid image for Droplet creation.")
		return
	case float64:
		image := api.db.GetImage(int64(image_key))
		if image == nil {
			api.unprocessableEntity(response, "You specified an invalid image for Droplet creation.")
			return
		}
		droplet.Image = *image
	default:
		api.unprocessableEntity(response, "You specified an invalid image for Droplet creation.")
		return
	}

	for i := range createDroplet.SSHKeys {
		switch key_key := createDroplet.SSHKeys[i].(type) {
		case int64:
			if key := api.db.GetSSHKey(key_key); key == nil {
				api.unprocessableEntity(response, "You specified an invalid ssh key for Droplet creation.")
				return
			}
		case string:
			if key := api.db.GetSSHKeyByFingerprint(key_key); key == nil {
				api.unprocessableEntity(response, "You specified an invalid ssh key for Droplet creation.")
				return
			}
		default:
			api.unprocessableEntity(response, "You specified an invalid ssh key for Droplet creation.")
			return
		}
	}

	droplet.Tags = createDroplet.Tags
	if droplet.Tags == nil {
		droplet.Tags = []string{}
	}

	if createDroplet.VPCUUID == "" {
		droplet.VPCUUID = api.db.GetDefaultVPC().ID
	} else {
		if vpc := api.db.GetVPC(createDroplet.VPCUUID); vpc == nil {
			api.notFound(response, "Failed to resolve VPC")
			return
		}
		droplet.VPCUUID = createDroplet.VPCUUID
	}

	if createDroplet.Backups {
		api.unprocessableEntity(response, "Backups are not supported by this simulator.")
		return
	}
	if createDroplet.IPv6 {
		api.unprocessableEntity(response, "IPv6 is not supported by this simulator.")
		return
	}
	if createDroplet.PrivateNetworking {
		api.unprocessableEntity(response, "Private networking is not supported by this simulator.")
		return
	}
	if createDroplet.Monitoring {
		api.unprocessableEntity(response, "Monitoring is not supported by this simulator.")
		return
	}
	if len(createDroplet.Volumes) != 0 {
		api.unprocessableEntity(response, "Volumes are not supported by this simulator.")
		return
	}

	round, err := time.ParseDuration("1s")
	if err != nil {
		panic(err)
	}
	droplet.CreatedAt = time.Now().Round(round)
	droplet.Status = core.DropletStatusNew
	droplet.Networks.V4 = []core.DropletNetwork{}
	droplet.Networks.V6 = []core.DropletNetwork{}
	droplet.BackupIDs = []int64{}
	droplet.SnapshotIDs = []int64{}
	droplet.Features = []string{}
	droplet.VolumeIDs = []int64{}

	droplet = api.db.AddDroplet(&droplet)

	go func(id int64) {
		time.Sleep(5 * time.Second)
		droplet := api.db.GetDroplet(id)
		if droplet != nil {
			droplet.Status = core.DropletStatusActive
			privateNetwork, err := randomNetwork("192.168.1.1/24", core.DropletNetworkTypePrivate)
			if err != nil {
				panic(err)
			}
			publicNetwork, err := randomNetwork("10.0.0.1/8", core.DropletNetworkTypePublic)
			if err != nil {
				panic(err)
			}
			droplet.Networks.V4 = []core.DropletNetwork{
				*privateNetwork,
				*publicNetwork,
			}
			api.db.UpdateDroplet(droplet)
		}
	}(droplet.ID)

	api.log.Printf("There are now %d droplets.", api.db.CountDroplets())

	result := struct {
		Droplet core.Droplet `json:"droplet"`
	}{
		Droplet: droplet,
	}
	err = api.writeJSONResponse(response, http.StatusAccepted, result)
	if err != nil {
		api.internalError(response)
		return
	}
}

func (api *APIImpl) DeleteDroplet(ctx context.Context, response http.ResponseWriter, request *http.Request, ps httprouter.Params) {
	dropletIdStr := ps.ByName("droplet_id")
	dropletId, err := strconv.ParseInt(dropletIdStr, 10, 64)
	if err != nil {
		api.log.Printf("Invalid droplet id: %s\n", dropletIdStr)
		response.WriteHeader(http.StatusNotFound)
		return
	}
	droplet := api.db.DeleteDroplet(dropletId)
	if droplet == nil {
		api.log.Printf("Unknown droplet: %d\n", dropletId)
		response.WriteHeader(http.StatusNotFound)
		return
	}

	api.log.Printf("There are now %d droplets.", api.db.CountDroplets())

	response.WriteHeader(http.StatusNoContent)
}

func (api *APIImpl) GetDroplet(ctx context.Context, response http.ResponseWriter, request *http.Request, ps httprouter.Params) {
	dropletIdStr := ps.ByName("droplet_id")
	dropletId, err := strconv.ParseInt(dropletIdStr, 10, 64)
	if err != nil {
		api.log.Printf("Invalid droplet id: %s\n", dropletIdStr)
		response.WriteHeader(http.StatusNotFound)
		return
	}
	droplet := api.db.GetDroplet(dropletId)
	if droplet == nil {
		api.log.Printf("Unknown droplet: %d\n", dropletId)
		response.WriteHeader(http.StatusNotFound)
		return
	}

	result := struct {
		Droplet core.Droplet `json:"droplet"`
	}{
		Droplet: *droplet,
	}
	err = api.writeJSONResponse(response, http.StatusAccepted, result)
	if err != nil {
		api.internalError(response)
		return
	}
}

func (api *APIImpl) ListDroplets(ctx context.Context, response http.ResponseWriter, request *http.Request, _ httprouter.Params) {
	droplets := api.db.GetDroplets()
	result, err := paginate(DropletList(droplets), "droplets", request)
	if err != nil {
		api.internalError(response)
		return
	}
	err = api.writeJSONResponse(response, http.StatusOK, result)
	if err != nil {
		api.internalError(response)
		return
	}
}
