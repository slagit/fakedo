package api

import (
	"fmt"
	"net/http"
	"net/url"
	"strconv"

	"gitlab.com/slagit/fakedo/core"
)

type DomainList []core.Domain

func (l DomainList) Len() int {
	return len(l)
}

func (l DomainList) Slice(start, end int) interface{} {
	return l[start:end]
}

type DomainRecordList []core.DomainRecord

func (l DomainRecordList) Len() int {
	return len(l)
}

func (l DomainRecordList) Slice(start, end int) interface{} {
	return l[start:end]
}

type DropletList []core.Droplet

func (l DropletList) Len() int {
	return len(l)
}

func (l DropletList) Slice(start, end int) interface{} {
	return l[start:end]
}

type ImageList []core.Image

func (l ImageList) Len() int {
	return len(l)
}

func (l ImageList) Slice(start, end int) interface{} {
	return l[start:end]
}

type SSHKeyList []core.SSHKey

func (l SSHKeyList) Len() int {
	return len(l)
}

func (l SSHKeyList) Slice(start, end int) interface{} {
	return l[start:end]
}

type VPCList []core.VPC

func (l VPCList) Len() int {
	return len(l)
}

func (l VPCList) Slice(start, end int) interface{} {
	return l[start:end]
}

type List interface {
	Len() int
	Slice(start, end int) interface{}
}

func getPage(request *http.Request) (int, error) {
	var page int = 1

	rawPage := request.URL.Query().Get("page")
	if rawPage != "" {
		var err error
		page, err = strconv.Atoi(rawPage)
		if err != nil {
			return 0, err
		}
	}

	if page < 1 {
		return 0, fmt.Errorf("Invalid page: %d", page)
	}

	return page - 1, nil
}

func getPerPage(request *http.Request) (int, error) {
	var perPage int = 20

	rawPerPage := request.URL.Query().Get("per_page")
	if rawPerPage != "" {
		var err error
		perPage, err = strconv.Atoi(rawPerPage)
		if err != nil {
			return 0, err
		}
	}

	if perPage < 1 {
		return 0, fmt.Errorf("Invalid per_page: %d", perPage)
	}

	if perPage > 200 {
		return 0, fmt.Errorf("Invalid per_page: %d", perPage)
	}

	return perPage, nil
}

func pageLink(request *http.Request, page int) string {
	root, _ := url.Parse(fmt.Sprintf("https://%s/", request.Host))

	url := root.ResolveReference(request.URL)

	q := url.Query()
	if page > 0 {
		q.Set("page", strconv.Itoa(page+1))
	} else {
		q.Del("page")
	}
	url.RawQuery = q.Encode()
	return url.String()
}

func paginate(items List, name string, request *http.Request) (map[string]interface{}, error) {
	pageSize, err := getPerPage(request)
	if err != nil {
		return nil, err
	}

	page, err := getPage(request)
	if err != nil {
		return nil, err
	}

	start := page * pageSize
	if start > 0 && start >= items.Len() {
		return nil, fmt.Errorf("Page out of range: %d", page)
	}
	end := start + pageSize
	if end > items.Len() {
		end = items.Len()
	}

	portion := items.Slice(start, end)
	response := map[string]interface{}{
		name: portion,
		"meta": map[string]interface{}{
			"total": end - start,
		},
	}

	pages := map[string]string{}
	if start > 0 {
		pages["first"] = pageLink(request, 0)
		pages["prev"] = pageLink(request, page-1)
	}
	if end < items.Len() {
		pages["next"] = pageLink(request, page+1)
		lastPage := ((items.Len()+pageSize-1)/pageSize - 1)
		pages["last"] = pageLink(request, lastPage)
	}
	if len(pages) > 0 {
		response["links"] = map[string]interface{}{
			"pages": pages,
		}
	}

	return response, nil
}
