package dnsapi

import (
	"bytes"
	"encoding/binary"
	"log"
	"net"
	"strings"

	"gitlab.com/slagit/fakedo/db"
	"gitlab.com/slagit/fakedo/dns"
)

func ARData(fields ...string) ([]byte, error) {
	return net.ParseIP(fields[0]).To4(), nil
}

func TXTRData(fields ...string) ([]byte, error) {
	var buf bytes.Buffer
	for _, field := range fields {
		fdata := []byte(field)
		err := binary.Write(&buf, binary.BigEndian, uint8(len(fdata)))
		if err != nil {
			return []byte{}, err
		}
		err = binary.Write(&buf, binary.BigEndian, fdata)
		if err != nil {
			return []byte{}, err
		}
	}
	return buf.Bytes(), nil
}

type DNSImpl struct {
	log *log.Logger
	db  *db.Database
}

func New(log *log.Logger, db *db.Database) *DNSImpl {
	return &DNSImpl{
		log: log,
		db:  db,
	}
}

func (d DNSImpl) Handler(w dns.Writer, p *dns.Packet) {
	p.Flags |= dns.FlagAuthoritative | dns.FlagResponse
	p.Flags &^= dns.FlagRecursionAvailable

	for _, q := range p.Question {
		if q.QClass != dns.ClassIN {
			d.log.Printf("Unsupported QClass in question: %d\n", q.QClass)
			return
		}
		qname := strings.Join(q.Names, ".")
		qtype := dns.TypeNames[q.QType]
		for domain, records := range d.db.DomainRecords {
			for _, record := range records {
				var rname string
				if record.Name == "@" {
					rname = domain
				} else {
					rname = record.Name + "." + domain
				}
				if strings.ToLower(qname) == strings.ToLower(rname) && qtype == record.Type {
					var rdata []byte
					var err error
					switch record.Type {
					case "A":
						rdata, err = ARData(record.Data)
					case "TXT":
						rdata, err = TXTRData(record.Data)
					default:
						d.log.Printf("Unexpected record type: %s\n", record.Type)
						return
					}
					if err != nil {
						d.log.Printf("Error encoding RData: %s\n", err)
						return
					}
					p.Answer = append(p.Answer, dns.Resource{
						Names: strings.Split(rname, "."),
						ResourceFlags: dns.ResourceFlags{
							Type:  q.QType,
							Class: q.QClass,
							TTL:   *record.TTL,
						},
						RData: rdata,
					})
				}
			}
		}
		if len(p.Answer) == 0 {
			p.Flags |= dns.RCodeNXDomain
		}
	}

	err := w.Write(p)
	if err != nil {
		d.log.Printf("Error writing DNS packet: %s\n", err)
	}
}
