package core

import (
	"time"
)

type Domain struct {
	Name string `json:"name"`
	TTL  uint32 `json:"ttl"`
}

type DomainRecord struct {
	ID       int64   `json:"id"`
	Type     string  `json:"type"`
	Name     string  `json:"name"`
	Data     string  `json:"data"`
	Priority *int    `json:"priority"`
	Port     *int    `json:"port"`
	TTL      *uint32 `json:"ttl"`
	Weight   *int    `json:"weight"`
	Flags    *int    `json:"flags"`
	Tag      *string `json:"tag"`
}

const (
	DropletNetworkTypePublic  = "public"
	DropletNetworkTypePrivate = "private"
)

type DropletNetwork struct {
	IPAddress string `json:"ip_address"`
	Netmask   string `json:"netmask"`
	Gateway   string `json:"gateway"`
	Type      string `json:"type"`
}

const (
	DropletStatusNew    = "new"
	DropletStatusActive = "active"
)

type Droplet struct {
	ID          int64     `json:"id"`
	Name        string    `json:"name"`
	Memory      int64     `json:"memory"`
	VCPUs       int64     `json:"vcpus"`
	Disk        int64     `json:"disk"`
	Locked      bool      `json:"locked"`
	CreatedAt   time.Time `json:"created_at"`
	Status      string    `json:"status"`
	BackupIDs   []int64   `json:"backup_ids"`
	SnapshotIDs []int64   `json:"snapshot_ids"`
	Features    []string  `json:"features"`
	Region      Region    `json:"region"`
	Image       Image     `json:"image"`
	Size        Size      `json:"size"`
	SizeSlug    string    `json:"size_slug"`
	Networks    struct {
		V4 []DropletNetwork
		V6 []DropletNetwork
	} `json:"networks"`
	Kernel           *struct{} `json:"kernel"`
	NextBackupWindow *struct{} `json:"next_backup_window"`
	Tags             []string  `json:"tags"`
	VolumeIDs        []int64   `json:"volume_ids"`
	VPCUUID          string    `json:"vcpu_uuid"`
}

const (
	ImageStatusNew       = "NEW"
	ImageStatusPending   = "pending"
	ImageStatusAvailable = "available"
)

const (
	ImageTypeCustom = "custom"
)

type Image struct {
	CreatedAt    time.Time `json:"created_at"`
	Description  string    `json:"description"`
	Distribution string    `json:"distribution"`
	ErrorMessage string    `json:"error_message"`
	ID           int64     `json:"id"`
	Name         string    `json:"name"`
	Region       string    `json:"region,omitempty"`
	Regions      []string  `json:"regions"`
	Status       string    `json:"status"`
	Tags         []string  `json:"tags"`
	Type         string    `json:"type"`
	URL          string    `json:"url,omitempty"`
	UpdatedAt    time.Time `json:"updated_at"`
}

type Region struct {
	Name      string   `json:"name"`
	Slug      string   `json:"slug"`
	Features  []string `json:"features"`
	Available bool     `json:"available"`
	Sizes     []string `json:"sizes"`
}

type Size struct {
	Slug         string   `json:"slug"`
	Memory       int64    `json:"memory"`
	VCPUs        int64    `json:"vcpus"`
	Disk         int64    `json:"disk"`
	Transfer     int64    `json:"transfer"`
	PriceMonthly int64    `json:"price_monthly"`
	PriceHourly  float64  `json:"price_hourly"`
	Regions      []string `json:"regions"`
	Available    bool     `json:"available"`
}

type SSHKey struct {
	ID          int64  `json:"id"`
	Fingerprint string `json:"fingerprint"`
	Name        string `json:"name"`
	PublicKey   string `json:"public_key"`
}

type VPC struct {
	CreatedAt   time.Time `json:"created_at"`
	Default     bool      `json:"default"`
	Description string    `json:"description"`
	ID          string    `json:"id"`
	URN         string    `json:"urn"` // do:vpc:<id>
	Name        string    `json:"name"`
	Region      string    `json:"region"`
	IPRange     string    `json:"ip_range"`
}
