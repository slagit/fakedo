package dns

import (
	"bytes"
	"log"
	"net"
	"runtime/debug"
)

type Handler interface {
	ServeDNS(Writer, *Packet)
}

type HandlerFunc func(Writer, *Packet)

func (f HandlerFunc) ServeDNS(w Writer, p *Packet) {
	f(w, p)
}

func serve(pc net.PacketConn, addr net.Addr, buf []byte, handler Handler) {
	defer func() {
		err := recover()
		if err != nil {
			log.Printf("dns: panic serving %s: %s\n%s", addr, err, string(debug.Stack()))
		}
	}()

	r := NewReader(bytes.NewReader(buf))

	p, err := r.Read()
	if err != nil {
		panic(err)
	}

	var w bytes.Buffer
	handler.ServeDNS(NewWriter(&w), p)
	_, err = pc.WriteTo(w.Bytes(), addr)
	if err != nil {
		panic(err)
	}
}

func ListenAndServe(addr string, handler Handler) error {
	pc, err := net.ListenPacket("udp", addr)
	if err != nil {
		return err
	}
	defer pc.Close()

	for {
		buf := make([]byte, 4096)
		n, addr, err := pc.ReadFrom(buf)
		if err != nil {
			return err
		}

		go serve(pc, addr, buf[:n], handler)
	}
}
