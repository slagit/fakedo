package dns

import (
	"encoding/binary"
	"io"
)

type Reader interface {
	Read() (*Packet, error)
}

type FullHeader struct {
	Header
	QDCount uint16
	ANCount uint16
	NSCount uint16
	ARCount uint16
}

type ReaderImpl struct {
	reader io.Reader
}

func NewReader(r io.Reader) Reader {
	return ReaderImpl{
		reader: r,
	}
}

func (r ReaderImpl) read(data interface{}) error {
	return binary.Read(r.reader, binary.BigEndian, data)
}

func (r ReaderImpl) readUInt8() (retval uint8, err error) {
	err = r.read(&retval)
	return
}

func (r ReaderImpl) readUInt16() (retval uint16, err error) {
	err = r.read(&retval)
	return
}

func (r ReaderImpl) readData() ([]byte, error) {
	length, err := r.readUInt16()
	if err != nil {
		return nil, err
	}
	retval := make([]byte, length)
	err = r.read(&retval)
	if err != nil {
		return nil, err
	}
	return retval, nil
}

func (r ReaderImpl) readName() (string, error) {
	length, err := r.readUInt8()
	if err != nil {
		return "", err
	}
	rawName := make([]byte, length)
	err = r.read(&rawName)
	if err != nil {
		return "", err
	}
	return string(rawName), nil
}

func (r ReaderImpl) readNames() ([]string, error) {
	retval := make([]string, 0)

	for {
		name, err := r.readName()
		if err != nil {
			return nil, err
		}
		if name == "" {
			return retval, nil
		}
		retval = append(retval, name)
	}
}

func (r ReaderImpl) readHeader() (retval FullHeader, err error) {
	err = r.read(&retval)
	return
}

func (r ReaderImpl) readQuestion() (*Question, error) {
	var retval Question
	var err error
	retval.Names, err = r.readNames()
	if err != nil {
		return nil, err
	}
	err = r.read(&retval.QuestionFooter)
	if err != nil {
		return nil, err
	}
	return &retval, nil
}

func (r ReaderImpl) readQuestions(count uint16) ([]Question, error) {
	retval := make([]Question, count)
	for i := range retval {
		res, err := r.readQuestion()
		if err != nil {
			return nil, err
		}
		retval[i] = *res
	}
	return retval, nil
}

func (r ReaderImpl) readResource() (*Resource, error) {
	var retval Resource
	var err error
	retval.Names, err = r.readNames()
	if err != nil {
		return nil, err
	}
	err = r.read(&retval.ResourceFlags)
	if err != nil {
		return nil, err
	}
	retval.RData, err = r.readData()
	if err != nil {
		return nil, err
	}
	return &retval, nil
}

func (r ReaderImpl) readResources(count uint16) ([]Resource, error) {
	retval := make([]Resource, count)
	for i := range retval {
		res, err := r.readResource()
		if err != nil {
			return nil, err
		}
		retval[i] = *res
	}
	return retval, nil
}

func (r ReaderImpl) Read() (*Packet, error) {
	var retval Packet
	fh, err := r.readHeader()
	if err != nil {
		return nil, err
	}

	retval.Header = fh.Header
	retval.Question, err = r.readQuestions(fh.QDCount)
	if err != nil {
		return nil, err
	}
	retval.Answer, err = r.readResources(fh.ANCount)
	if err != nil {
		return nil, err
	}
	retval.Authority, err = r.readResources(fh.NSCount)
	if err != nil {
		return nil, err
	}
	retval.Additional, err = r.readResources(fh.ARCount)
	if err != nil {
		return nil, err
	}
	return &retval, nil
}

type Writer interface {
	Write(packet *Packet) error
}

type WriterImpl struct {
	writer io.Writer
}

func NewWriter(w io.Writer) Writer {
	return WriterImpl{
		writer: w,
	}
}

func (w WriterImpl) write(data interface{}) error {
	return binary.Write(w.writer, binary.BigEndian, data)
}

func (w WriterImpl) writeUInt8(value uint8) error {
	return w.write(value)
}

func (w WriterImpl) writeUInt16(value uint16) error {
	return w.write(value)
}

func (w WriterImpl) writeData(data []byte) error {
	length := uint16(len(data))
	err := w.writeUInt16(length)
	if err != nil {
		return err
	}
	return w.write(data)
}

func (w WriterImpl) writeName(name string) error {
	rawName := []byte(name)
	length := uint8(len(rawName))
	err := w.writeUInt8(length)
	if err != nil {
		return err
	}
	return w.write(rawName)
}

func (w WriterImpl) writeNames(names []string) error {
	for _, name := range names {
		err := w.writeName(name)
		if err != nil {
			return err
		}
	}
	return w.writeName("")
}

func (w WriterImpl) writeHeader(header *FullHeader) error {
	return w.write(header)
}

func (w WriterImpl) writeQuestion(question *Question) error {
	err := w.writeNames(question.Names)
	if err != nil {
		return err
	}
	return w.write(&question.QuestionFooter)
}

func (w WriterImpl) writeQuestions(questions []Question) error {
	for i := range questions {
		err := w.writeQuestion(&questions[i])
		if err != nil {
			return err
		}
	}
	return nil
}

func (w WriterImpl) writeResource(resource *Resource) error {
	err := w.writeNames(resource.Names)
	if err != nil {
		return err
	}
	err = w.write(&resource.ResourceFlags)
	if err != nil {
		return err
	}
	return w.writeData(resource.RData)
}

func (w WriterImpl) writeResources(resources []Resource) error {
	for i := range resources {
		err := w.writeResource(&resources[i])
		if err != nil {
			return err
		}
	}
	return nil
}

func (w WriterImpl) Write(packet *Packet) error {
	fh := FullHeader{
		Header:  packet.Header,
		QDCount: uint16(len(packet.Question)),
		ANCount: uint16(len(packet.Answer)),
		NSCount: uint16(len(packet.Authority)),
		ARCount: uint16(len(packet.Additional)),
	}
	err := w.writeHeader(&fh)
	if err != nil {
		return err
	}
	err = w.writeQuestions(packet.Question)
	if err != nil {
		return err
	}
	err = w.writeResources(packet.Answer)
	if err != nil {
		return err
	}
	err = w.writeResources(packet.Authority)
	if err != nil {
		return err
	}
	err = w.writeResources(packet.Additional)
	if err != nil {
		return err
	}
	return nil
}
