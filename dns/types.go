package dns

const (
	RCodeNXDomain uint16 = 0x0003

	FlagRecursionAvailable uint16 = 0x0080
	FlagRecursionDesired   uint16 = 0x0100
	FlagAuthoritative      uint16 = 0x0400
	FlagResponse           uint16 = 0x8000

	ClassIN uint16 = 1
)

var TypeNames = map[uint16]string{
	1:  "A",
	16: "TXT",
}

type Header struct {
	ID    uint16
	Flags uint16
}

type QuestionFooter struct {
	QType  uint16
	QClass uint16
}

type Question struct {
	Names []string
	QuestionFooter
}

type ResourceFlags struct {
	Type  uint16
	Class uint16
	TTL   uint32
}

type Resource struct {
	Names []string
	ResourceFlags
	RData []byte
}

type Packet struct {
	Header
	Question   []Question
	Answer     []Resource
	Authority  []Resource
	Additional []Resource
}
