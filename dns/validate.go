package dns

import (
	"errors"
	"fmt"
	"strings"
)

func ValidateName(n string) error {
	if len(n) < 1 {
		return errors.New("too short")
	}
	if len(n) > 63 {
		return errors.New("too long")
	}
	return nil
}

func Validate(n string) error {
	if len(n) > 255 {
		return errors.New("too long")
	}

	for i, p := range strings.Split(n, ".") {
		err := ValidateName(p)
		if err != nil {
			return fmt.Errorf("segment %d: %s", i, err)
		}
	}

	return nil
}
