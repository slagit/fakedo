# certs/

To re-create the certificates in this directory run:

```sh
minica -ca-cert fakedo.minica.pem \
       -ca-key fakedo.minica.key.pem \
       -domains localhost,fakedo \
       -ip-addresses 127.0.0.1
```

From the `test/certs/` directory after [installing
MiniCA](https://github.com/jsha/minica#installation)
