package db

import (
	"sync"
	"time"

	"github.com/google/uuid"
	"gitlab.com/slagit/fakedo/core"
)

type Database struct {
	Domains       []core.Domain
	DomainRecords map[string][]core.DomainRecord
	Droplets      []core.Droplet
	Images        []core.Image
	Mutex         sync.RWMutex
	SSHKeys       []core.SSHKey
	VPCs          []core.VPC
	id            int64
}

func (db *Database) nextID() int64 {
	db.id += 1
	return db.id
}

func (db *Database) AddDomain(domain *core.Domain) core.Domain {
	db.Mutex.Lock()
	defer db.Mutex.Unlock()

	db.Domains = append(db.Domains, *domain)
	return db.Domains[len(db.Domains)-1]
}

func (db *Database) AddImage(image *core.Image) core.Image {
	db.Mutex.Lock()
	defer db.Mutex.Unlock()

	db.Images = append(db.Images, *image)
	retval := &db.Images[len(db.Images)-1]
	retval.ID = db.nextID()
	return *retval
}

func (db *Database) AddDomainRecord(domain string, record *core.DomainRecord) core.DomainRecord {
	db.Mutex.Lock()
	defer db.Mutex.Unlock()

	records := db.DomainRecords[domain]
	records = append(records, *record)
	retval := &records[len(records)-1]
	retval.ID = db.nextID()
	db.DomainRecords[domain] = records
	return *retval
}

func (db *Database) AddDroplet(droplet *core.Droplet) core.Droplet {
	db.Mutex.Lock()
	defer db.Mutex.Unlock()

	db.Droplets = append(db.Droplets, *droplet)
	retval := &db.Droplets[len(db.Droplets)-1]
	retval.ID = db.nextID()
	return *retval
}

func (db *Database) AddSSHKey(key *core.SSHKey) core.SSHKey {
	db.Mutex.Lock()
	defer db.Mutex.Unlock()

	db.SSHKeys = append(db.SSHKeys, *key)
	retval := &db.SSHKeys[len(db.SSHKeys)-1]
	retval.ID = db.nextID()
	return *retval
}

func (db *Database) CountDomains() int {
	return len(db.Domains)
}

func (db *Database) CountDomainRecords() int {
	var retval int

	for _, v := range db.DomainRecords {
		retval += len(v)
	}

	return retval
}

func (db *Database) CountDroplets() int {
	return len(db.Droplets)
}

func (db *Database) CountImages() int {
	return len(db.Images)
}

func (db *Database) CountSSHKeys() int {
	return len(db.SSHKeys)
}

func (db *Database) GetDomain(name string) *core.Domain {
	db.Mutex.RLock()
	defer db.Mutex.RUnlock()

	for idx := range db.Domains {
		if db.Domains[idx].Name == name {
			retval := db.Domains[idx]
			return &retval
		}
	}

	return nil
}

func (db *Database) DeleteDomainRecord(domainname string, id int64) *core.DomainRecord {
	db.Mutex.RLock()
	defer db.Mutex.RUnlock()

	rl := db.DomainRecords[domainname]
	for i, r := range rl {
		if r.ID == id {
			retval := r
			copy(rl[i:], rl[i+1:])
			db.DomainRecords[domainname] = rl[:len(rl)-1]
			return &retval
		}
	}

	return nil
}

func (db *Database) DeleteDroplet(id int64) *core.Droplet {
	db.Mutex.RLock()
	defer db.Mutex.RUnlock()

	for i, r := range db.Droplets {
		if r.ID == id {
			retval := r
			copy(db.Droplets[i:], db.Droplets[i+1:])
			db.Droplets = db.Droplets[:len(db.Droplets)-1]
			return &retval
		}
	}

	return nil
}

func (db *Database) DeleteImage(id int64) *core.Image {
	db.Mutex.RLock()
	defer db.Mutex.RUnlock()

	for i, r := range db.Images {
		if r.ID == id {
			retval := r
			copy(db.Images[i:], db.Images[i+1:])
			db.Images = db.Images[:len(db.Images)-1]
			return &retval
		}
	}

	return nil
}

func (db *Database) GetDomainRecords(domainname string) []core.DomainRecord {
	db.Mutex.RLock()
	defer db.Mutex.RUnlock()

	records := db.DomainRecords[domainname]
	retval := make([]core.DomainRecord, len(records))
	copy(retval, records)
	return retval
}

func (db *Database) GetDomains() []core.Domain {
	db.Mutex.RLock()
	defer db.Mutex.RUnlock()

	retval := make([]core.Domain, len(db.Domains))
	copy(retval, db.Domains)
	return retval
}

func (db *Database) GetDroplet(id int64) *core.Droplet {
	db.Mutex.RLock()
	defer db.Mutex.RUnlock()

	for idx := range db.Droplets {
		if db.Droplets[idx].ID == id {
			retval := db.Droplets[idx]
			return &retval
		}
	}

	return nil
}

func (db *Database) GetDroplets() []core.Droplet {
	db.Mutex.RLock()
	defer db.Mutex.RUnlock()

	retval := make([]core.Droplet, len(db.Droplets))
	copy(retval, db.Droplets)
	return retval
}

func (db *Database) GetImage(id int64) *core.Image {
	db.Mutex.RLock()
	defer db.Mutex.RUnlock()

	for idx := range db.Images {
		if db.Images[idx].ID == id {
			retval := db.Images[idx]
			return &retval
		}
	}

	return nil
}

func (db *Database) GetImages() []core.Image {
	db.Mutex.RLock()
	defer db.Mutex.RUnlock()

	retval := make([]core.Image, len(db.Images))
	copy(retval, db.Images)
	return retval
}

func (db *Database) GetSSHKey(id int64) *core.SSHKey {
	db.Mutex.RLock()
	defer db.Mutex.RUnlock()

	for idx := range db.SSHKeys {
		if db.SSHKeys[idx].ID == id {
			retval := db.SSHKeys[idx]
			return &retval
		}
	}

	return nil
}

func (db *Database) GetSSHKeyByFingerprint(fingerprint string) *core.SSHKey {
	db.Mutex.RLock()
	defer db.Mutex.RUnlock()

	for idx := range db.SSHKeys {
		if db.SSHKeys[idx].Fingerprint == fingerprint {
			retval := db.SSHKeys[idx]
			return &retval
		}
	}

	return nil
}

func (db *Database) GetSSHKeys() []core.SSHKey {
	db.Mutex.RLock()
	defer db.Mutex.RUnlock()

	retval := make([]core.SSHKey, len(db.SSHKeys))
	copy(retval, db.SSHKeys)
	return retval
}

func (db *Database) GetVPC(id string) *core.VPC {
	db.Mutex.RLock()
	defer db.Mutex.RUnlock()

	for idx := range db.VPCs {
		if db.VPCs[idx].ID == id {
			retval := db.VPCs[idx]
			return &retval
		}
	}

	return nil
}

func (db *Database) GetDefaultVPC() *core.VPC {
	db.Mutex.RLock()
	defer db.Mutex.RUnlock()

	retval := db.VPCs[0]
	return &retval
}

func (db *Database) GetVPCs() []core.VPC {
	db.Mutex.RLock()
	defer db.Mutex.RUnlock()

	retval := make([]core.VPC, len(db.VPCs))
	copy(retval, db.VPCs)
	return retval
}

func (db *Database) UpdateDroplet(droplet *core.Droplet) *core.Droplet {
	db.Mutex.RLock()
	defer db.Mutex.RUnlock()

	for idx := range db.Droplets {
		if db.Droplets[idx].ID == droplet.ID {
			db.Droplets[idx] = *droplet
			retval := db.Droplets[idx]
			return &retval
		}
	}

	return nil
}

func (db *Database) UpdateImage(image *core.Image) *core.Image {
	db.Mutex.RLock()
	defer db.Mutex.RUnlock()

	for idx := range db.Images {
		if db.Images[idx].ID == image.ID {
			db.Images[idx] = *image
			retval := db.Images[idx]
			return &retval
		}
	}

	return nil
}

func New() *Database {
	uuid, err := uuid.NewRandom()
	if err != nil {
		panic(err)
	}
	round, err := time.ParseDuration("1s")
	if err != nil {
		panic(err)
	}
	db := &Database{
		DomainRecords: make(map[string][]core.DomainRecord),
		VPCs: []core.VPC{
			{
				CreatedAt: time.Now().Round(round),
				Default:   true,
				ID:        uuid.String(),
				URN:       "do:vpc:" + uuid.String(),
				Name:      "default-fake1",
				Region:    "fake1",
				IPRange:   "192.168.1.0/24",
			},
		},
	}
	return db
}
