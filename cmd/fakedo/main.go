package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path"
	"sync"

	"gitlab.com/slagit/fakedo/api"
	"gitlab.com/slagit/fakedo/db"
	"gitlab.com/slagit/fakedo/dns"
	"gitlab.com/slagit/fakedo/dnsapi"
)

type config struct {
	FakeDO struct {
		DNSAddress    string
		ListenAddress string
		Certificate   string
		PrivateKey    string
	}
}

func failOnError(err error, msg string) {
	if err != nil {
		fmt.Fprintf(os.Stderr, "%s: %s\n", msg, err)
		os.Exit(1)
	}
}

func main() {
	configFile := flag.String(
		"config",
		"test/config/fakedo-config.json",
		"File path to the FakeDO configuration file",
	)
	flag.Parse()
	if *configFile == "" {
		flag.Usage()
		os.Exit(1)
	}

	logger := log.New(os.Stdout, "FakeDO ", log.LstdFlags)
	logger.Printf("Starting Fake Digital Ocean server\n")

	var c config
	err := readConfigFile(*configFile, &c)
	failOnError(err, "Reading JSON config file into config structure")

	db := db.New()

	wg := new(sync.WaitGroup)
	wg.Add(2)

	go func() {
		dnsImpl := dnsapi.New(logger, db)
		dnsHandler := dnsImpl.Handler

		logger.Printf("Listening for DNS requests on: %s\n", c.FakeDO.DNSAddress)
		log.Fatal(dns.ListenAndServe(c.FakeDO.DNSAddress, dns.HandlerFunc(dnsHandler)))
		wg.Done()
	}()

	go func() {
		apiImpl := api.New(logger, db)
		muxHandler := apiImpl.Handler()

		logger.Printf("Listening for API requests on: %s\n", c.FakeDO.ListenAddress)
		log.Fatal(http.ListenAndServeTLS(
			c.FakeDO.ListenAddress,
			c.FakeDO.Certificate,
			c.FakeDO.PrivateKey,
			muxHandler,
		))
		wg.Done()
	}()

	wg.Wait()
}

func readConfigFile(filename string, out interface{}) error {
	configData, err := ioutil.ReadFile(path.Clean(filename))
	if err != nil {
		return err
	}
	return json.Unmarshal(configData, out)
}
